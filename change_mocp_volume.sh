#!/bin/bash

# If /tmp/currentVolume doesn't exist
if [[ ! -f /tmp/currentVolume ]]; then
    # Create it, and put 25 as the contents
    echo "25" > /tmp/currentVolume
fi
# If the parameter is 'up'
if [[ "$1" == "up" ]]; then
    # Set vol to the ammount specified in /tmp/currentVolume
	vol="$(cat /tmp/currentVolume)"
    # Add 2 to 'vol'
	newVol=$(echo $vol+2 | bc -l)
    # Use this handy tool to increase the volume
	~/scripts/pa_volume/pa_volume "OSS Emulation[mocp]" $newVol
    # Write the new volume to the file
	echo $newVol > /tmp/currentVolume
# If the parameter is 'down'
elif [[ "$1" == "down" ]]; then
    # Set vol to the ammount specified in /tmp/currentVolume
	vol="$(cat /tmp/currentVolume)"
    # If 'vol' > 0
	if [[ "$vol" -ge 0 ]]; then
        # Subtract 2 from 'vol'
		newVol=$(echo $vol-2 | bc -l)
	fi
    # Use the nice script to change volume
	~/scripts/pa_volume/pa_volume "OSS Emulation[mocp]" $newVol
    # Write the new volume to /tmp/currentVolume
	echo $newVol > /tmp/currentVolume
fi
