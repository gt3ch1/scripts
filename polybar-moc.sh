#!/bin/sh
##########
# UNUSED #
##########

if [ "$(mocp -Q %state 2> /dev/null)" != "STOP" ];then
    SONG=$(mocp -Q %song 2> /dev/null)
    if [ -n "$SONG" ]; then
        echo "$SONG - $(mocp -Q %artist 2> /dev/null )"
    else
        basename "$(mocp -Q %file 2> /dev/null)"
    fi
else
    echo ""
fi
