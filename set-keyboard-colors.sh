#!/bin/bash

row1=(esc f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12)
row2=(grave 1 2 3 4 5 6 7 8 9 0 minus equal bspace ins home pgup numslash numstar numminus)
row3=(tab q w e r t y u i o p lbrace rbrance bslash del end pgdn num7 num8 num9 numplus)
row4=(a s d f g h j k l colon quote enter num4 num5 num6)
row5=(lshift z x c v b n m comma dot slash ro rshift up num1 num2 num3 numenter)
row6=(lctrl lwin lalt space ralt rwin rmenu rctrl left down right num0 numdot)
bg=$(cat ~/.config/i3/config | grep background | head -n1 | cut -d ' ' -f 3)
# data=$(convert $bg -crop '1x1+100+200' txt:- | \
#    sed -n '1!p' | cut -d ' ' -f 4 | cut -d '#' -f 2);
currentX=0
currentY=100
for t in ${row1[@]}; do
    currentX=$(echo $currentX+120 | bc -l)
    data=$(convert $bg -crop '1x1+'$currentX'+200' txt:- | \
        sed -n '1!p' | cut -d ' ' -f 4 | cut -d '#' -f 2);
    echo "rgb $t:ff$data" > /tmp/ckbpipe000
done

currentX=0
currentY=230
for t in ${row2[@]}; do
    currentX=$(echo $currentX+80 | bc -l)
    data=$(convert $bg -crop '1x1+'$currentX'+'$currentY txt:- | \
        sed -n '1!p' | cut -d ' ' -f 4 | cut -d '#' -f 2);
    echo "rgb $t:ff$data" > /tmp/ckbpipe000
done

currentX=0
currentY=430
for t in ${row3[@]}; do
    currentX=$(echo $currentX+75 | bc -l)
    data=$(convert $bg -crop '1x1+'$currentX'+'$currentY txt:- | \
        sed -n '1!p' | cut -d ' ' -f 4 | cut -d '#' -f 2);
    echo "rgb $t:ff$data" > /tmp/ckbpipe000
done

currentX=0
currentY=630
for t in ${row4[@]}; do
    currentX=$(echo $currentX+80 | bc -l)
    data=$(convert $bg -crop '1x1+'$currentX'+'$currentY txt:- | \
        sed -n '1!p' | cut -d ' ' -f 4 | cut -d '#' -f 2);
    echo "rgb $t:ff$data" > /tmp/ckbpipe000
done

currentX=0
currentY=830
for t in ${row5[@]}; do
    currentX=$(echo $currentX+85 | bc -l)
    data=$(convert $bg -crop '1x1+'$currentX'+'$currentY txt:- | \
        sed -n '1!p' | cut -d ' ' -f 4 | cut -d '#' -f 2);
    echo "rgb $t:ff$data" > /tmp/ckbpipe000
done

currentX=0
currentY=1040
for t in ${row6[@]}; do
    currentX=$(echo $currentX+85 | bc -l)
    data=$(convert $bg -crop '1x1+'$currentX'+'$currentY txt:- | \
        sed -n '1!p' | cut -d ' ' -f 4 | cut -d '#' -f 2);
    echo "rgb $t:ff$data" > /tmp/ckbpipe000
done
