#!/bin/bash

# Starts/Restarts compton

# If compoton is running
if [[ $(pgrep compton) ]];
then
    # Kill it; sleep; and then restart it
    killall compton 2>/dev/null && sleep 1; compton --config ~/.config/i3/compton.conf;
else
    # start it
    compton --config ~/.config/i3/compton.conf;
fi
