#!/bin/bash

# Gets the current state from MOCP
state=$(mocp -Q %state 2>/dev/null)
# If music is currently playing
if [[ "$state" == "PLAY" ]]; then
    # Echo Font Awesome's Pause icon
	echo " ";
else
    # Echo Font Awesom's Play icon
	echo " ";
fi
